Cours 1 24/01/2023
   Lors du cours, nous avons abordé le sujet des branches et des merges. Lorsque j'ai commencé à travailler sur mon devoir, la chose qui m'a posé le plus de difficultés était le merge des deux branches après avoir effectué des modifications. De plus, je n'ai pas pu comprendre pourquoi, mais je n'ai pas rencontré de conflit de fusion comme prévu dans l'exercice 3 de la feuille d'exercices. Par conséquent, j'ai omis cet exercice (car il n'y avait aucune modification à apporter) et j'ai poursuivi à partir de l'exercice 4.


